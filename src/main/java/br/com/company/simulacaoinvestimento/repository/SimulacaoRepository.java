package br.com.company.simulacaoinvestimento.repository;

import br.com.company.simulacaoinvestimento.model.Simulacao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SimulacaoRepository extends JpaRepository<Simulacao, Integer> {


}

