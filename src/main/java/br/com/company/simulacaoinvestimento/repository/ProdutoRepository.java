package br.com.company.simulacaoinvestimento.repository;

import br.com.company.simulacaoinvestimento.model.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.xml.ws.BindingType;

public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

    Produto findById(int id);


}
