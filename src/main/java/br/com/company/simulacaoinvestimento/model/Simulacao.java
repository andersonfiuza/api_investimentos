package br.com.company.simulacaoinvestimento.model;


import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

@Entity
@Table(name = "tb_simulacao")

public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "favor informar o nome do cliente!")
    @NotBlank(message = "Favor informar o nome do cliente!")
    private String nome;
    @Email(message = "Favor informar um email valido")
    private String email;

    @Digits(integer = 4, fraction = 2)
    private double valor;

    private int quantidadedemeses;
    private double rendimentopormes;
    private double montante;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRendimentopormes(double rendimentopormes) {
        this.rendimentopormes = rendimentopormes;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public double getRendimentopormes() {
        return rendimentopormes;
    }

    public double getMontante() {
        return montante;
    }

    public int getQuantidadedemeses() {
        return quantidadedemeses;
    }

    public void setQuantidadedemeses(int quantidadedemeses) {
        this.quantidadedemeses = quantidadedemeses;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Produto> produtos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }


    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }


}
