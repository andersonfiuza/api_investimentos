package br.com.company.simulacaoinvestimento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimulacaoinvestimentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimulacaoinvestimentoApplication.class, args);
	}

}
