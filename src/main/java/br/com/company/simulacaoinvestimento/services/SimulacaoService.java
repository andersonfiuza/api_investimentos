package br.com.company.simulacaoinvestimento.services;

import br.com.company.simulacaoinvestimento.DTO.ResumoSimulacaoDTO;

import br.com.company.simulacaoinvestimento.model.Produto;
import br.com.company.simulacaoinvestimento.model.Simulacao;
import br.com.company.simulacaoinvestimento.repository.ProdutoRepository;
import br.com.company.simulacaoinvestimento.repository.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;

@Service
public class SimulacaoService {


    @Autowired
    ProdutoRepository produtoRepository;
    @Autowired
    SimulacaoRepository simulacaoRepository;

    @Autowired
    ProdutoService produtoService;


    public Iterable<Simulacao> lerTodoAsSimulacoes() {


        List<Simulacao> simulacaoList = simulacaoRepository.findAll();

        if (simulacaoList.isEmpty()) {
            throw new RuntimeException("Nenhuma simulacão econtrada");
        } else {
            return simulacaoList;
        }
    }


    public ResumoSimulacaoDTO realizacaoSimulacao(int id, Simulacao simulacao) {

        Produto produto = produtoService.buscaProdutoId(id);

        Simulacao simula = calculaJurosCompostos(produto, simulacao);

        ResumoSimulacaoDTO resumoSimulacaoDTO = new ResumoSimulacaoDTO();

        resumoSimulacaoDTO.setMontante(simula.getMontante());

        resumoSimulacaoDTO.setRendimentopormes(simula.getRendimentopormes());

        simulacaoRepository.save(simulacao);
        return  resumoSimulacaoDTO;


    }

    public Simulacao calculaJurosCompostos(Produto produto, Simulacao simulacao) {
        //Calculo dos juros compostos
        //Formula  M = C (1+i)t
        double calcula = (1 + produto.getRendimento());
        double juros = Math.pow(calcula, simulacao.getQuantidadedemeses());
        double montante = (simulacao.getValor() * juros);

        //Formata os juros
        String jurosformatado = new DecimalFormat("##.00").format(juros);
        simulacao.setRendimentopormes(Double.parseDouble(jurosformatado.replaceAll(",", ".")));
        //Formata o montante
        String montanteformatado = new DecimalFormat("##.00").format(montante);
        simulacao.setMontante(Double.parseDouble(montanteformatado.replaceAll(",", ".")));

        return simulacao;

    }


}
