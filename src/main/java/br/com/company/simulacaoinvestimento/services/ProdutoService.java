package br.com.company.simulacaoinvestimento.services;

import br.com.company.simulacaoinvestimento.model.Produto;
import br.com.company.simulacaoinvestimento.repository.ProdutoRepository;
import br.com.company.simulacaoinvestimento.repository.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProdutoService {

    @Autowired
    SimulacaoRepository simulacaoRepository;
    @Autowired
    ProdutoRepository produtoRepository;

    public Produto buscaProdutoId(int id) {

       return produtoRepository.findById(id);

    }

}
