package br.com.company.simulacaoinvestimento.DTO;

public class ResumoSimulacaoDTO {


    private double rendimentopormes;
    private double montante;

    public ResumoSimulacaoDTO() {
    }

    public double getRendimentopormes() {
        return rendimentopormes;
    }

    public void setRendimentopormes(double rendimentopormes) {
        this.rendimentopormes = rendimentopormes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }
}
