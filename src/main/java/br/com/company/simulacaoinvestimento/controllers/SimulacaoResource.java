package br.com.company.simulacaoinvestimento.controllers;


import br.com.company.simulacaoinvestimento.DTO.ResumoSimulacaoDTO;
import br.com.company.simulacaoinvestimento.model.Produto;
import br.com.company.simulacaoinvestimento.model.Simulacao;

import br.com.company.simulacaoinvestimento.services.SimulacaoService;
import org.hibernate.annotations.GeneratorType;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/simuladorinvestimento")
public class SimulacaoResource {
    @Autowired
    private SimulacaoService simulacaoService;


    private ResumoSimulacaoDTO resumoSimulacaoDTO;


    @PostMapping("/simulainvestimento/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResumoSimulacaoDTO salvarinvestimento(@RequestBody @Valid Simulacao simulacao, @PathVariable(name = "id") int id) {

        try {
            ResumoSimulacaoDTO simula = simulacaoService.realizacaoSimulacao(id, simulacao);

            return simula;

        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @GetMapping("/listarsimulacoes")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Simulacao> ListaTodasAsSimulacoes() {

        try {
            return simulacaoService.lerTodoAsSimulacoes();
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }



}
