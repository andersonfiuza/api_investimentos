package br.com.company.simulacaoinvestimento.controllers;
import br.com.company.simulacaoinvestimento.model.Produto;
import br.com.company.simulacaoinvestimento.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value="/simuladorinvestimento")
public class ProdutoResource {

    @Autowired
    ProdutoRepository produtoRepository;

    @GetMapping("/listaprodutos")
    @ResponseStatus(HttpStatus.OK)
    public List<Produto> listaDeProdutos(){
        return  produtoRepository.findAll();
    }

    @GetMapping("/produto/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Produto produto(@PathVariable(value = "id") int id){
        return  produtoRepository.findById(id);
    }

    @PostMapping("/cadastraproduto")
    @ResponseStatus(HttpStatus.CREATED)
    public   Produto salvarProduto(@RequestBody @Valid Produto produto) {
        return  produtoRepository.save(produto);

    }

}
